import React, {useState, useEffect} from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';

import AddProducts from '../pages/AddProducts';

export default function ProductForm(){
    const [product, setProduct] = useState({
      name: [],
      text: [],
      image_alt: [],
      isEdited: false,
      index: ''
    });

    const [name, setName] = useState('');
    const [text, setText] = useState('');
    const [imageAlt, setImageAlt] = useState('');
    const [edit, setEdit] = useState(true);
    const [editName, setEditName] = useState('');
    const [index, setIndex] = useState('');

    function showName(e){
      setName(e.target.value);
      // setProduct((prevProduct) => ({
      //   ...prevProduct,
      //   info: {name: name}
      // }))
    }

    function showText(e){
      setText(e.target.value);
      // setProduct({...product, info: {text: text}})
    }

    function showImageAlt(e){
      setImageAlt(e.target.value);
      // setProduct((prevProduct) => ({
      //   ...prevProduct,
      //   info: {imagealt: imageAlt}
      // }))
    }

    // console.log(product)

    function createProduct(e){
      e.preventDefault();
      if(name === "" || text === "" || imageAlt === ""){
        alert('Fields required');
      }
      else{
        // info.push({name: name})
        // console.log(name);
        // console.log(list_text);
        // console.log(list_alt);

        // setProduct((prevProduct) => ({
        //   ...prevProduct,
        //   info: {name: name, text: text, image_alt: imageAlt}
        // }))
        // console.log(info);
        alert('Record added')
        // const list = product.info;
        // list.push(name);
        // setProduct((prevProduct) => ({
        //   ...prevProduct,
        //   info: list
        // }))
        const list_name = product.name;
        const list_text = product.text;
        const list_alt = product.image_alt;

        list_name.push(name);
        list_text.push(text);
        list_alt.push(imageAlt);

        setProduct((prevProduct) => ({
          ...prevProduct,
          name: list_name,
          text: list_text,
          image_alt: list_alt
        }));
      }
      console.log(product);
    }

    const deleteProduct = (index) => {
      const list_name = product.name;
      const list_text = product.text;
      const list_alt = product.image_alt;
      console.log(list_name)
      list_name.splice(index, 1);
      list_text.splice(index, 1);
      list_alt.splice(index, 1);
      console.log(product);
      setProduct((prevProduct) => ({
        ...prevProduct,
        name: list_name,
        text: list_text,
        image_alt: list_alt
      }))
      alert('Removed')
    }

    function editButton(index){
      setProduct((prevProduct) => ({
        ...prevProduct,
        isEdited: edit,
        index: index
      }))
      setIndex(index);
      console.log(index)
    }

    function editProduct(e){
      setEditName(e.target.value);
      console.log(editName);
    }

    function acceptButton(index){
      const list = product.name;
      list[index] = editName;
      // console.log(product.name[index])
      setProduct((prevProduct) => ({
        ...prevProduct,
        name: list
      }))
      alert('Product name changed');
      console.log(product)
    }

    function cancelProduct(){
      setProduct((prevProduct) => ({
        ...prevProduct,
        isEdited: false
      }))
    }

    return (
          <>
            <Form className="productForm" onSubmit={createProduct}>
              <Row>
                <Col sm={12} md={6}>
                  <Form.Group className="mb-3" controlId="formBasicEmail">
                    <Form.Label>Name</Form.Label>
                    <Form.Control type="name" placeholder="Enter name" onChange={showName}/>
                  </Form.Group>    
                </Col>
                
                <Col sm={12} md={6}>
                  <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Text</Form.Label>
                    <Form.Control type="text" placeholder="Enter text" onChange={showText}/>
                  </Form.Group>
                </Col>

                <Col sm={12} md={12}>
                  <Form.Group className="mb-3" controlId="formBasicPassword">
                    <Form.Label>Image alt</Form.Label>
                    <Form.Control type="text" placeholder="Enter image alt" onChange={showImageAlt}/>
                  </Form.Group>

                  <Button variant="success" type="submit">
                    Add
                  </Button>
                </Col>
              </Row>
            </Form>

            <h3 className="mt-5">Added Products:</h3>
            {
              product.name.length ? 
              product.name.map((product, index) => {
                return(
                    <AddProducts key={index} index={index} name={product} edit={editButton} remove={deleteProduct} />
                  )
              }) :
              <div>No records found</div>
            }
            {
              product.isEdited === true ?
              <>
                {
                  product.name.length ?
                        <div>
                          <h3 htmlFor="name">Name: {product.name[index]}</h3>
                          <input type="text" id="name" placeholder="Change name" onChange={editProduct} style={{color: "black"}}/>
                          <Button variant="info" size="sm" className="ms-2 me-2" onClick={()=>acceptButton(index)}>Accept</Button>
                          <Button variant="danger" size="sm" onClick={cancelProduct}>Cancel</Button>
                        </div>
                      
                  :
                  <h3>Clear</h3>
                }
              </>
              :
              <>
                <h3>Nothing to edit</h3>
              </>
            }
          </>
    )
}